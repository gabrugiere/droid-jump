# <img src="data/icon.svg" alt="Bugdroid, la mascote d'Android" width=40> Droid Jump

Droid Jump est une application mobile pour Android basé sur le jeu tant renommé : Doodle Jump.

## Description
Ici, le but du jeu est de faire sauter Bugdroid, la mascotte d'Android, de plate-forme en plate forme le plus haut possible, et sans... tomber dans le vide. Plus haut vous montez, plus vous gagnez de points. Montez ainsi le plus loin possible et battez votre propre score. Ou celui des autres, car vous avez en bonus la possibilité de choisir un pseudo afin de distinguer des joueurs.

## Captures d'écran
<img src="data/screenshots/home.png" alt="Home" width=250>
<img src="data/screenshots/play.png" alt="In game" width=250>
<img src="data/screenshots/options.png" alt="Options" width=250>
<img src="data/screenshots/scores.png" alt="Scores" width=250>
<img src="data/screenshots/gameOver.png" alt="Game over" width=250>

## Caractéristiques
- [X] Sauter de plate-forme en plate forme
- [X] Sauvegarde des scores (affichage des 10 meilleurs)
- [X] Musiques et bruitages désactivables
- [X] Choix d'un pseudo

## Download
Droid Jump est disponible en téléchargement sur le Play Store [ici](https://play.google.com/store/apps/details?id=iut.clermont.DroidJump).
