package iut.clermont.DroidJump.model.character;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.List;
import java.util.Random;

import iut.clermont.DroidJump.R;

public class Plateforme extends Entite{
    private boolean estMarquee;

    public Plateforme(int positionX, int positionY,int hauteur, int largeur, Bitmap bitmapEntite, boolean estMarquee) {
        this.positionX=positionX;
        this.positionY=positionY;
        this.hauteur=hauteur;
        this.largeur=largeur;
        this.bitmapEntite= bitmapEntite;
        this.estMarquee=estMarquee;
    }

    public Plateforme(){

    }

    public void sePlacerSurLAxeDesAbscissesAleatoirement(int debutIntervalle, int finIntervalle){
        Random random =new Random();
        this.positionX=random.nextInt(finIntervalle-debutIntervalle)+1;
    }

    public Plateforme[] creerListeDePlateformes(int nombreDePlateformes){
        Plateforme listeDePlateformes[]=new Plateforme[nombreDePlateformes];
        return listeDePlateformes;
    }

    public boolean isEstMarquee() {
        return estMarquee;
    }

    public void setEstMarquee(boolean estMarquee) {
        this.estMarquee = estMarquee;
    }


}
