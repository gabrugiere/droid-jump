package iut.clermont.DroidJump.model.character;

import android.graphics.Bitmap;

public abstract class Entite {


    protected int positionX;
    protected int positionY;
    protected Bitmap bitmapEntite;
    protected int hauteur;
    protected int largeur;



    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public int getHauteur(){
        return hauteur;
    }

    public void setHauteur(int hauteur){
        this.hauteur=hauteur;
    }

    public int getLargeur(){
        return largeur;
    }

    public void setLargeur(int largeur){
        this.largeur=largeur;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public Bitmap getBitmapEntite() {
        return bitmapEntite;
    }

    public void setBitmapEntite(Bitmap bitmapEntite) {
        this.bitmapEntite = bitmapEntite;
    }
}
