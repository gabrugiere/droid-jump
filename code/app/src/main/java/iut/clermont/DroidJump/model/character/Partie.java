package iut.clermont.DroidJump.model.character;

import iut.clermont.DroidJump.model.Score;

public class Partie {
    private boolean gameOver;

    public Partie(boolean gameOver) {
        this.gameOver = gameOver;

    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }


}
