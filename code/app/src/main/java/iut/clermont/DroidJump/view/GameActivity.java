package iut.clermont.DroidJump.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

import iut.clermont.DroidJump.R;
import iut.clermont.DroidJump.database.DatabaseHandler;
import iut.clermont.DroidJump.model.Score;

public class GameActivity extends AppCompatActivity implements SensorEventListener {
    private GameView gameView;
    private TextView yourScore;

    private MediaPlayer game_music;
    private SharedPreferences settings;
    private boolean song;
    private SensorManager sensorManager;
    private Sensor accelerometerSensor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Create Gyroscope sensor
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // Keep the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Get the GameView
        gameView = findViewById(R.id.gameView);
        yourScore = findViewById(R.id.your_score);

        yourScore.setText(getString(R.string.scoreInGame, gameView.getScore()));

        settings = this.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
        song = settings.getBoolean("SONG_GAME_Bool", true);

        if (game_music == null) {
            game_music = MediaPlayer.create(this, R.raw.game_music);
            game_music.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }

        if (game_music != null && song) {
            new Thread(new Runnable() {
                public void run() {
                    game_music.start();
                }
            }).start();
        }

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                GameActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (gameView.getIsGameOver()) {
                            cancel();
                            Intent gameOverActivity = new Intent(GameActivity.this, GameOverActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt("SCORE", gameView.getScore());
                            gameOverActivity.putExtras(bundle);
                            startActivity(gameOverActivity);
                            finish();
                        }
                        yourScore.setText(getString(R.string.scoreInGame, gameView.getScore()));
                    }
                });
            }
        };

        timer.schedule(task, 0, 16);
    }

    @Override
    public void onResume() {
        sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_GAME);
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent gameActivity = new Intent(GameActivity.this, MainActivity.class);
        startActivity(gameActivity);
        finish();
    }

    @Override
    public void onPause() {
        sensorManager.unregisterListener(this);
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences settings = this.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
        DatabaseHandler db = new DatabaseHandler(this);
        db.addScore(new Score(1, gameView.getScore(), settings.getString("PSEUDO", null)));

        if (game_music != null) {
            game_music.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (game_music != null) {
            game_music.release();
            game_music = null;
        }
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            gameView.setDoodleX(sensorEvent.values[0]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
