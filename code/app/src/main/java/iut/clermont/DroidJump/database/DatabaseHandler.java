package iut.clermont.DroidJump.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import iut.clermont.DroidJump.model.Score;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final String TABLE_NAME = "scores";

    private static final String COLUMN_KEY = "id";
    private static final String COLUMN_SCORE = "score";
    private static final String COLUMN_PSEUDO = "pseudo";

    private static final String DATABASE_NAME = "scores.db";
    private static final int DATABASE_VERSION = 1;

    private static final String SCORE_TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_SCORE + " REAL NOT NULL, " +
                    COLUMN_PSEUDO + " TEXT NOT NULL);";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    private static final String SQL_GET_TEN_SCORES =
            "SELECT * FROM " + TABLE_NAME + " ORDER BY " + COLUMN_SCORE + " DESC LIMIT " + 10;

    private static final String SQL_GET_BEST_SCORE =
            "SELECT * FROM " + TABLE_NAME + " ORDER BY " + COLUMN_SCORE + " DESC LIMIT " + 1;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SCORE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * Add a score in the database
     * @param score Score to be added
     */
    public void addScore(Score score) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(COLUMN_SCORE, score.getScore());
        value.put(COLUMN_PSEUDO, score.getPseudo());
        db.insert(TABLE_NAME, null, value);
        db.close();
    }

    /**
     * Get the ten best scores of the database
     * @return A list of ten scores
     */
    public List<Score> tenBestScores() {
        List<Score> scores = new LinkedList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_TEN_SCORES, null);
        Score score = null;

        if (cursor.moveToFirst()) {
            do {
                score = new Score();
                score.setId(Integer.parseInt(cursor.getString(0)));
                score.setScore(Integer.parseInt(cursor.getString(1)));
                score.setPseudo(cursor.getString(2));
                scores.add(score);
            } while (cursor.moveToNext());
        }

        return scores;
    }

    /**
     * Get the best score of the database
     * @return The best score
     */
    public Score bestScore() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_BEST_SCORE, null);
        Score score = null;

        if (cursor.moveToFirst()) {
            do {
                score = new Score();
                score.setId(Integer.parseInt(cursor.getString(0)));
                score.setScore(Integer.parseInt(cursor.getString(1)));
                score.setPseudo(cursor.getString(2));
            } while (cursor.moveToNext());
        }

        return score;
    }
}
