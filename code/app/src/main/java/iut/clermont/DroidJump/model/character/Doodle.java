package iut.clermont.DroidJump.model.character;

import android.graphics.Bitmap;

public class Doodle extends Entite {

    public Doodle(int positionX, int positionY,int hauteur, int largeur, Bitmap bitmapEntite) {
        this.positionX=positionX;
        this.positionY=positionY;
        this.hauteur=hauteur;
        this.largeur=largeur;
        this.bitmapEntite= bitmapEntite;
    }

    public void seDeplacerSurLaxeDesAbscisses(double value) {
        if(value > 0.5f){
            if(positionX<-75){
                positionX=600;
            }
            positionX=positionX-7;
        } else if (value < -0.5f){
            if(positionX>600){
                positionX=-75;
            }
            positionX=positionX+7;
        }
    }
}
