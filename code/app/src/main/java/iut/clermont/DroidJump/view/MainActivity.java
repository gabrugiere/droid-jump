package iut.clermont.DroidJump.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

import iut.clermont.DroidJump.R;
import iut.clermont.DroidJump.view.fragment.DialogPseudoPopUpFragment;
import iut.clermont.DroidJump.view.fragment.MainFragmentCallBack;
import iut.clermont.DroidJump.view.fragment.MainMenuFragment;
import iut.clermont.DroidJump.view.fragment.ScoresFragment;
import iut.clermont.DroidJump.view.fragment.SettingsFragment;

public class MainActivity extends AppCompatActivity implements MainFragmentCallBack {
    private MediaPlayer home_music;
    private MediaPlayer button_click;
    private int elapsedTime = 0;
    private boolean song = true;
    private boolean sound = true;

    private FragmentManager fragmentManager;
    private MainMenuFragment mainMenuFragment;
    private SettingsFragment settingsFragment;
    private ScoresFragment scoresragment;

    private DialogPseudoPopUpFragment pseudoPopUpFragment;

    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        settings = this.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);

        fragmentManager = getSupportFragmentManager();
        mainMenuFragment = new MainMenuFragment();
        settingsFragment = new SettingsFragment();
        scoresragment = new ScoresFragment();

        if (savedInstanceState != null) {
            elapsedTime = savedInstanceState.getInt("ELAPSED_TIME", 0);
        }

        if (settings.getString("PSEUDO", null) == null) {
            pseudoPopUpFragment = new DialogPseudoPopUpFragment();
            pseudoPopUpFragment.show(fragmentManager, "Pseudo Pop-up");
        }

        fragmentManager.beginTransaction()
                .add(R.id.fragmentContainer, mainMenuFragment)
                .commit();

        if (home_music == null) {
            home_music = MediaPlayer.create(this, R.raw.home_music);
            home_music.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }

        /**
         * Create a thread to play music in background
         */
        if (home_music != null) {
            new Thread(new Runnable() {
                public void run() {
                    home_music.start();
                    home_music.seekTo(elapsedTime);
                }
            }).start();
        }

        if (button_click == null) {
            button_click = MediaPlayer.create(this, R.raw.button_click);
            button_click.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        setOptions();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setOptions();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setOptions();
    }

    public void playGame(View view) {
        Intent gameActivity = new Intent(MainActivity.this, GameActivity.class);
        playSoundButton();
        startActivity(gameActivity);
        finish();
    }

    public void options(View view) {
        playSoundButton();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, settingsFragment)
                .addToBackStack(null)
                .commit();
    }

    public void scores(View view) {
        playSoundButton();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, scoresragment)
                .addToBackStack(null)
                .commit();
    }

    public void quit(View view){
        playSoundButton();
        finish();
    }

    /**
     * Get preferences and set play or pause the music
     */
    private void setOptions() {
        song = settings.getBoolean("SONG_HOME_Bool", true);
        sound = settings.getBoolean("SOUND_Bool", true);


        if (!home_music.isPlaying() && song) {
            home_music.start();
        }
        if (!song){
            home_music.pause();
        }
    }

    /**
     * Play a song when we click on a button
     */
    private void playSoundButton() {
        if (button_click != null && sound) {
            new Thread(new Runnable() {
                public void run() {
                    button_click.start();
                }
            }).start();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (home_music != null) {
            outState.putInt("ELAPSED_TIME", home_music.getCurrentPosition());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (home_music != null) {
            home_music.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (home_music != null) {
            home_music.release();
            home_music = null;
        }
    }

    /**
     * Reload the settings fragment if the player set its pseudo in settings
     */
    @Override
    public void onItemSelected() {
        if (fragmentManager.findFragmentById(R.id.fragmentContainer) == settingsFragment) {
            fragmentManager.beginTransaction()
                    .remove(settingsFragment)
                    .replace(R.id.fragmentContainer, settingsFragment)
                    .commit();
        }
    }
}
