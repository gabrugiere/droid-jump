package iut.clermont.DroidJump.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import iut.clermont.DroidJump.R;
import iut.clermont.DroidJump.model.Score;

public class ScoreAdapter extends ArrayAdapter<Score> {
    public ScoreAdapter(Context context, int simple_list_item_1, List<Score> scores) {
        super(context, 0, scores);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_scores,parent, false);
        }

        ScoreViewHolder viewHolder = (ScoreViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new ScoreViewHolder();
            viewHolder.rank = convertView.findViewById(R.id.rank);
            viewHolder.crown = convertView.findViewById(R.id.crown);
            viewHolder.score = convertView.findViewById(R.id.score);
            viewHolder.pseudo = convertView.findViewById(R.id.pseudo);
            convertView.setTag(viewHolder);
        }

        Score score = getItem(position);

        int rank = position+1;

        viewHolder.rank.setText(Integer.toString(rank));
        switch (rank) {
            case 1:
                viewHolder.crown.setImageResource(R.drawable.first_place);
                break;
            case 2:
                viewHolder.crown.setImageResource(R.drawable.second_place);
                break;
            case 3:
                viewHolder.crown.setImageResource(R.drawable.third_place);
                break;
            default:
                viewHolder.crown.setImageResource(R.drawable.basic_place);
                break;
        }
        viewHolder.score.setText(Integer.toString(score.getScore()));
        viewHolder.pseudo.setText(score.getPseudo());

        return convertView;
    }

    private class ScoreViewHolder{
        TextView rank;
        ImageView crown;
        TextView score;
        TextView pseudo;
    }
}
