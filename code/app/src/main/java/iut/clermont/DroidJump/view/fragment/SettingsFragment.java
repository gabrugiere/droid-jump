package iut.clermont.DroidJump.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import iut.clermont.DroidJump.R;

public class SettingsFragment extends Fragment {
    private Context ctx;

    private Switch musicIsPlayHome;
    private Switch musicIsPlayInGame;
    private Switch soundsIsPlay;
    private ImageView editPseudo;
    private TextView pseudo;

    private SharedPreferences settings;


    private FragmentManager fragmentManager;

    private DialogPseudoPopUpFragment pseudoPopUpFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        musicIsPlayHome = view.findViewById(R.id.switchMusicHome);
        musicIsPlayInGame = view.findViewById(R.id.switchMusicGame);
        soundsIsPlay = view.findViewById(R.id.switchSounds);
        editPseudo = view.findViewById(R.id.editPseudo);
        pseudo = view.findViewById(R.id.pseudoShow);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        ctx = getContext();

        fragmentManager = getActivity().getSupportFragmentManager();

        settings = ctx.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
        musicIsPlayHome.setChecked(settings.getBoolean("SONG_HOME_Bool", true));
        musicIsPlayInGame.setChecked(settings.getBoolean("SONG_GAME_Bool", true));
        soundsIsPlay.setChecked(settings.getBoolean("SOUND_Bool", true));
        pseudo.setText(settings.getString("PSEUDO", null));

        editPseudo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pseudoPopUpFragment = new DialogPseudoPopUpFragment();
                pseudoPopUpFragment.show(fragmentManager, "Pseudo Pop-up");
            }
        });

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onPause() {
        settings = ctx.getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("SONG_HOME_Bool", musicIsPlayHome.isChecked());
        editor.putBoolean("SONG_GAME_Bool", musicIsPlayInGame.isChecked());
        editor.putBoolean("SOUND_Bool", soundsIsPlay.isChecked());
        editor.commit();

        Toast.makeText(ctx, R.string.settingsSaved,Toast.LENGTH_LONG).show();

        super.onPause();
    }
}
