package iut.clermont.DroidJump.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import java.util.List;

import iut.clermont.DroidJump.R;
import iut.clermont.DroidJump.database.DatabaseHandler;
import iut.clermont.DroidJump.model.Score;
import iut.clermont.DroidJump.view.adapter.ScoreAdapter;
import iut.clermont.DroidJump.view.fragment.DialogPseudoPopUpFragment;
import iut.clermont.DroidJump.view.fragment.MainFragmentCallBack;
import iut.clermont.DroidJump.view.fragment.MainMenuFragment;
import iut.clermont.DroidJump.view.fragment.ScoresFragment;
import iut.clermont.DroidJump.view.fragment.SettingsFragment;

public class GameOverActivity extends AppCompatActivity {
    private int score = 0;
    private TextView yourScore;
    private TextView bestScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        yourScore = findViewById(R.id.your_score);
        bestScore = findViewById(R.id.best_score);

        if (bundle != null) {
            score = bundle.getInt("SCORE");
        }
        yourScore.setText(Integer.toString(score));

        DatabaseHandler db = new DatabaseHandler(this);

        Score score = db.bestScore();

        if (score != null) {
            bestScore.setText(Integer.toString(score.getScore()));
        }
    }

    public void rePlayGame(View view) {
        Intent gameActivity = new Intent(GameOverActivity.this, MainActivity.class);
        startActivity(gameActivity);
        finish();
    }
}
